from rest_framework import routers

from . import views

app_name = "books"

router = routers.SimpleRouter()
router.register("authors", views.AuthorModelViewSet)
router.register("books", views.BookModelViewSet)


urlpatterns = [] + router.urls
