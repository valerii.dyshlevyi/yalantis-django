from django.urls import path
from .views import CeleryAPIView

app_name = 'dialogs'

urlpatterns = [
    # path("start_task/<str:value>/", CeleryAPIView.as_view(), name="start_task"),
    path("start_task/<int:value>/", CeleryAPIView.as_view(), name="start_task"),
]
