from apps.dialogs.utils import TaskControl
from yalantis_django.celery import app
import time


@app.task(bind=True, max_retries=3)
def debug_task(self, value: int):
    print("debug_task")
    if value > 0:
        print(f"Retry {value}")
        self.retry(countdown=2 ** self.request.retries)
    print(value)
    # with TaskControl(f_name=name) as task_control:
    #     print(f"Start of: {name}")
    #     time.sleep(30)
    #     print(f"End of: {name}")


@app.task
def play_me(self):
    print("Let's party!!!")
