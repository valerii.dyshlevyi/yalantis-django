from rest_framework import views, status
from rest_framework import permissions
from rest_framework.response import Response

from .tasks import debug_task
from . utils import TaskControl


class CeleryAPIView(views.APIView):
    http_method_names = ["get"]
    permission_classes = (permissions.AllowAny,)

    def get(self, request, value, *args, **kwargs):
        # if TaskControl.check_task_control(f_name=value):
        #     return Response(status=status.HTTP_400_BAD_REQUEST)
        import time
        time.sleep(30)
        # debug_task.delay(value)
        return Response({}, status=status.HTTP_200_OK)
