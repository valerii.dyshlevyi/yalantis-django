import time
import redis

r = redis.Redis()


class TaskControl(object):
    """ Save to specific Redis key that function is working """

    def __init__(self, f_name: str, *args, **kwargs):
        self.f_name: str = f_name

    def __enter__(self):
        # Mark that function is still working
        print(f"{self.f_name} enter in Redis: ")
        r.setex(
            name='task_control:{}'.format(self.f_name),
            time=30,
            value=self.f_name
        )

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(f"{self.f_name} exit from Redis: ")

    @classmethod
    def check_task_control(cls, f_name):
        is_busy = r.get(name='task_control:{}'.format(f_name))
        print(f"Check that {f_name} is busy -> {is_busy}")
        return is_busy
