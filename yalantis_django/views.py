from django.http import HttpRequest, HttpResponse
from django.shortcuts import render


def contact(request: HttpRequest) -> HttpResponse:
    tag = "about-us"
    who = "alex"
    job = "Junior Python Developer"
    email = "alexandr.pichkurov@gmail.com"
    return render(
        request,
        "contact.html",
        {"tag": tag, "who": who, "job": job, "email": email},
    )


def about_us(request: HttpRequest) -> HttpResponse:
    about = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    return render(request, "about-us.html", {"about": about})


def author(request: HttpRequest) -> HttpResponse:
    author = "Oleksandr Pichkurov"
    return render(request, "author.html", {"author": author})


def index(request: HttpRequest) -> HttpResponse:
    return render(request, "index.html")
