"""yalantis_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.conf import settings
from django.contrib import admin
from django.urls import include, path

from . import views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("contact/", views.contact, name="contact"),
    path("about-us/", views.about_us, name="about_us"),
    path("author/", views.author, name="author"),
    path("", views.index, name="index"),
    path("accounts/", include("apps.accounts.urls", namespace="account")),
    path("dialogs/", include('apps.dialogs.urls', namespace="dialogs")),
    path("books/", include('apps.books.urls', namespace="books")),
]

if settings.DEBUG:
    urlpatterns += (path("__debug__/", include(debug_toolbar.urls)),)
